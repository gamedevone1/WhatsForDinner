extends Control

onready var BgMusic: AudioStreamPlayer = $Bg/Music
onready var BgMusicAnimPlayer: AnimationPlayer = $Bg/Music/AnimationPlayer


func _ready():
	PlayBgMusic()
	yield(get_tree().create_timer(10.0), "timeout")
	StopBgMusic()
	SceneManager.TransitionTo("res://Source/Main.tscn")


func PlayBgMusic():
	BgMusicAnimPlayer.play("FadeIn")
	BgMusic.volume_db = -80
	BgMusic.play()


func StopBgMusic():
	BgMusicAnimPlayer.play_backwards("FadeIn")
	yield(BgMusicAnimPlayer, "animation_finished")

