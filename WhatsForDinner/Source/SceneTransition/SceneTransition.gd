extends CanvasLayer

# Emitted when  transition screen is ready to swap scenes
signal Transitioned

# Type of transition
enum TYPE{
	FADEIN,
	FADEOUT
}

# Reference to background
onready var Bg: ColorRect = $Bg

# Reference to the _AnimationPlayer_ node
onready var anim_player: AnimationPlayer = $Bg/AnimationPlayer


func Transition(TransitionType: int) -> void:
	# Plays the Fade animation and wait until it finishes
	match TransitionType:
		TYPE.FADEIN:
			anim_player.play("Fade")
			yield(anim_player, "animation_finished")
			emit_signal("Transitioned")
		TYPE.FADEOUT:
			anim_player.play_backwards("Fade")
		_:
			print_debug(name + " Error: Invalid transition type selected. Selected: " + str(TransitionType))
			
	
	
