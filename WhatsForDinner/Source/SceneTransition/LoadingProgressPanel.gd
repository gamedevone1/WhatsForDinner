extends CanvasLayer

onready var LoadingBar: ProgressBar = $Bg/VBox/ProgressBar
onready var Bg: Panel = $Bg
onready var Twn: Tween = $Bg/VBox/ProgressBar/Tween
onready var ResourceName: Label = $Bg/VBox/Name

func Show():
	Bg.show()

func Hide():
	Bg.hide()

func SetValue(new_value: float) -> void:
	Twn.interpolate_property(LoadingBar,
								"value",
								LoadingBar.value,
								new_value,0.1,
								Tween.TRANS_CUBIC,
								Tween.EASE_OUT)
	LoadingBar.value = new_value
