extends Node

onready var SceneTrans: CanvasLayer = $SceneTransition
onready var CurrentScene: Node = $CurrentScene
onready var LoadingBar: CanvasLayer = $LoadingProgressPanel

var _NextScenePath: String = ""


func _ready():
	LoadingBar.Hide()

func TransitionTo(ScenePath: String) -> void:
	SceneTrans.Transition(SceneTrans.TYPE.FADEIN)
	_NextScenePath = ScenePath


func _on_SceneTransition_Transitioned():
	if CurrentScene.get_child_count() >= 1:
		for child in CurrentScene.get_children():
			child.queue_free()
			yield(child, "tree_exited")
	
	var Loader: ResourceInteractiveLoader = ResourceLoader.load_interactive(_NextScenePath)
	
	LoadingBar.Show()
	
	while true:
		var Err = Loader.poll()
		if Err == ERR_FILE_EOF:
			var Resrc = Loader.get_resource()
			CurrentScene.call_deferred("add_child", Resrc.instance())
			LoadingBar.Hide()
			break
		if Err == OK:
			var Progress = float(Loader.get_stage()) / Loader.get_stage_count()
			LoadingBar.SetValue(Progress * 100)
		yield(get_tree(), "idle_frame")
	
	#var instance = load(_NextScenePath).instance()
	#CurrentScene.add_child(instance)
	
	SceneTrans.Transition(SceneTrans.TYPE.FADEOUT)
